# Stuff

## Alias
### Temporary

    alias ssha='eval $(ssh-agent) && ssh-add'

Remove alias

    unalias alias_name
    unalias -a [remove all alias]

### Permanent

    Bash – ~/.bashrc
    ZSH – ~/.zshrc
    Fish – ~/.config/fish/config.fish

    source ~/.bashrc