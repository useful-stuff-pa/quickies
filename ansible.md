# Ansible

## Adhoc
    #Basic example
    ansible testing -i inventory/host.yaml -m ping -u root

    ansible all --list-hosts

    ansible all --limit  entrypoint2.chungus.network  -m ping
    #Become root and ask for password
    ansible all -m apt -a name=vim-nox --become --ask-become-pass

    # Update Packages
    ansible -i inventory/host.yaml mail -m apt -a "update_cache=yes upgrade=dist"

### Download and install DEB package
    # APT
    ansible -i inventory/host.yaml malebolge-kubernetes -m apt -a "update_cache=yes name=nfs-common state=latest"
    
    #deb file
    ansible all -i inventory/testing.yaml -m get_url -a "url=https://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_10.8.4_all.deb dest=/tmp/phoronix-test-suite_10.8.4_all.deb" 
    
    ansible all -i inventory/testing.yaml -m apt -a "deb=/tmp/phoronix-test-suite_10.8.4_all.deb state=present"


## Playbooks
